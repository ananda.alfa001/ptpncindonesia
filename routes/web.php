<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.index');
})->name('home');

/*!--bagian visi misi-->*/
Route::get('/visimisi', function () {
    return view('about.visimisi');
})->name('about.visimisi');

Route::get('/struktur', function () {
    return view('about.struktur');
})->name('about.struktur');

Route::get('/service', function () {
    return view('service');
})->name('home.servis');

Route::get('/product', function () {
    return view('product.product');
})->name('home.product');

/*!--bagian produk-->*/
Route::get('/fiberlasercutting1', function () {
    return view('product.fiberlasercutting1');
})->name('product.fiberlasercutting1');

Route::get('/fiberlasercutting2', function () {
    return view('product.fiberlasercutting2');
})->name('product.fiberlasercutting2');

Route::get('/plasmacutting1', function () {
    return view('product.plasmacutting1');
})->name('product.plasmacutting1');

Route::get('/plasmacutting2', function () {
    return view('product.plasmacutting2');
})->name('product.plasmacutting2');

Route::get('/router1', function () {
    return view('product.router1');
})->name('product.router1');

Route::get('/routeratcmultispindle', function () {
    return view('product.routeratcmultispindle');
})->name('product.routeratcmultispindle');

Route::get('/routeratc', function () {
    return view('product.routeratc');
})->name('product.routeratc');

Route::get('/plasmarotarycutting', function () {
    return view('product.plasmarotarycutting');
})->name('product.plasmarotarycutting');

/*!--bagian kontak-->*/
Route::get('/contact', function () {
    return view('contact.contact');
})->name('home.contact');



