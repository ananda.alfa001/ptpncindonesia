@extends('component.main')
@section('content')

<div class="page-breadcrumb bg-img space__bottom--r120" data-bg="assets/img/backgrounds/bc-bg.webp">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="page-breadcrumb-content text-center">
                    <h1>Kontak Kami</h1>
                    <ul class="page-breadcrumb-links">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li>Contact</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="conact-section space__bottom--r120">
    <div class="container">
        <div class="row">
            <div class="col space__bottom--40">
                <div class="contact-map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3585.8670797006566!2d112.69252611451995!3d-7.984276281826011!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd629dd0ed993f3%3A0x8f50bf20a01fb31b!2sPT%20Pioneer%20CNC%20Indonesia!5e1!3m2!1sid!2sid!4v1680005392014!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-12">
                <div class="contact-information">
                    <h4 class="space__bottom--30">Contact Information</h4>
                    <ul>
                        <li>
                            <span class="icon"><i class="fa fa-map-marker"></i></span>
                            <span class="text"><span>Jl. Raya Kedungrejo Genitri No.86, RT.001/RW.001, Genitri, Kedungrejo, Kec. Pakis, Kabupaten Malang, Jawa Timur 65154</span></span>
                        </li>
                        <li>
                            <span class="icon"><i class="fa fa-phone"></i></span>
                            <span class="text"><a href="https://api.whatsapp.com/send?phone=6282234936001">(+6282234936001)</a></span>
                        </li>
                        <li>
                            <span class="icon"><i class="fa fa-envelope-open"></i></span>
                            <span class="text"><a href="https://mail.google.com/mail/u/0/?tf=cm&fs=1&to=pt.pioneercncindonesiaofficial@gmail.com">pt.pioneercncindonesiaofficial@gmail.com</a>
                                <a href="{{route('home')}}">www.ptpioneercncindonesia.com</a></span>
                        </li>
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection