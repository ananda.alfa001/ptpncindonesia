<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PT PIONEER CNC INDONESIA</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="icon" href="assets/img/favicon.ico">
    <!-- CSS
		============================================ -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <!-- Flaticon CSS -->
    <link rel="stylesheet" href="assets/css/flaticon.min.css">
    <!-- Slick Slider CSS -->
    <link rel="stylesheet" href="assets/css/plugins/slick.min.css">
    <!-- CSS Animation CSS -->
    <link rel="stylesheet" href="assets/css/plugins/cssanimation.min.css">
    <!-- Justified Gallery CSS -->
    <link rel="stylesheet" href="assets/css/plugins/justifiedGallery.min.css">
    <!-- Light Gallery CSS -->
    <link rel="stylesheet" href="assets/css/plugins/light-gallery.min.css">
    <!-- Vendor & Plugins CSS (Please remove the comment from below vendor.min.css & plugins.min.css for better website load performance and remove css files from avobe) -->
    <!--
		<link rel="stylesheet" href="assets/css/vendor.min.css">
		<link rel="stylesheet" href="assets/css/plugins/plugins.min.css">
		-->
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
    <!--====================  header area ====================-->
    <div class="header-area header-sticky bg-img space__inner--y40 background-repeat--x background-color--dark d-none d-lg-block" data-bg="assets/img/icons/ruler.webp">
        <!-- header top -->
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="header-top-info">
                            <span class="header-top-info__image pr-1"><img width="20" height="19" src="assets/img/icons/phone.webp" alt=""></span>
                            <span class="header-top-info__text">0822-3493-6001</span>
                        </div>
                    </div>
                    <div class="col-lg-4">

                    </div>
                    <div class="col-lg-4">

                    </div>
                </div>
            </div>
        </div>
        <!-- menu bar -->
        <div class="menu-bar">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12  position-relative">
                        <div class="menu-bar-wrapper background-color--default space__inner--x35">
                            <div class="menu-bar-wrapper-inner">
                                <div class="row align-items-center">
                                    <div class="col-lg-2">
                                        <div class="brand-logo">
                                            <a href="index.html">
                                                <img width="142" height="31" src="assets/img/logo.webp" class="img-fluid" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-10">
                                        <div class="navigation-area d-flex justify-content-end align-items-center">
                                            <!-- navigation menu -->
                                            <nav class="main-nav-menu">
                                                <ul class="d-flex justify-content-end">
                                                    <li>
                                                        <a href="index.html">Home</a>

                                                    </li>
                                                    <li><a href="about.html">About Us</a></li>


                                                    <li class="has-sub-menu">
                                                        <a href="project.html">Produk</a>
                                                        <ul class="sub-menu">
                                                            <li><a href="project.html">Mesin CNC Fiber Laser Cutting</a></li>
                                                            <li><a href="project.html">Mesin CNC Plasma Cutting</a></li>
                                                            <li><a href="project.html">Mesin CNC Router Cutting</a></li>
                                                            <li><a href="project.html">Mesin CNC Rotary Cutting</a></li>
                                                            <li><a href="project.html">Mesin CNC Plano Milling</a></li>

                                                        </ul>
                                                    </li>
                                                    </li>
                                                    <li><a href="contact.html">Contact</a></li>
                                                </ul>
                                            </nav>
                                            <!-- search icon nav menu -->
                                            <div class="nav-search-icon">
                                                <button class="header-search-toggle"><i class="fa fa-search"></i></button>
                                                <div class="header-search-form">
                                                    <form action="#">
                                                        <input type="text" placeholder="Type and hit enter">
                                                        <button><i class="fa fa-search"></i></button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of header area  ====================-->
    <!--====================  mobile header ====================-->
    <div class="mobile-header header-sticky bg-img space__inner--y30 background-repeat--x background-color--dark d-block d-lg-none" data-bg="assets/img/icons/ruler.webp">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-6">
                    <div class="brand-logo">
                        <a href="index.html">
                            <img width="142" height="31" src="assets/img/logo-white.webp" class="img-fluid" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-6">
                    <div class="mobile-menu-trigger-wrapper text-end" id="mobile-menu-trigger">
                        <span class="mobile-menu-trigger"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of mobile header  ====================-->
    <!--====================  offcanvas mobile menu ====================-->
    <div class="offcanvas-mobile-menu" id="mobile-menu-overlay">
        <a href="javascript:void(0)" class="offcanvas-menu-close" id="mobile-menu-close-trigger">
            <span class="menu-close"></span>
        </a>
        <div class="offcanvas-wrapper">
            <div class="offcanvas-inner-content">
                <div class="offcanvas-mobile-search-area">
                    <form action="#">
                        <input type="search" placeholder="Search ...">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <nav class="offcanvas-navigation">
                    <ul>

                        <li><a href="#">Home</a></li>
                        <li><a href="about.html">About Us</a></li>

                        <li class="menu-item-has-children">
                            <a href="javascript:void(0)">Produk</a>
                            <ul class="sub-menu-mobile">
                                <li><a href="project.html">Mesin CNC Fiber Laser Cutting</a></li>
                                <li><a href="project.html">Mesin CNC Plasma Cutting</a></li>
                                <li><a href="project.html">Mesin CNC Router Cutting</a></li>
                                <li><a href="project.html">Mesin CNC Rotary Cutting</a></li>
                                <li><a href="project.html">Mesin CNC Plano Milling</a></li>
                            </ul>
                        </li>

                        </li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                </nav>
                <div class="offcanvas-widget-area">
                    <div class="off-canvas-contact-widget">
                        <div class="header-contact-info">
                            <ul class="header-contact-info__list">
                                <li><i class="fa fa-phone"></i> 0822-3493-6001</li>
                                <li><i class="fa fa-calendar"></i> Senin - Sabtu</li>
                                <br>
                                <li><i class="fa fa-clock-o"></i> 8.00 - 16.00</li>
                            </ul>
                        </div>
                    </div>
                    <!--Off Canvas Widget Social Start-->
                    <div class="off-canvas-widget-social">
                        <a href="#" title="Facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" title="Instagram"><i class="fa fa-instagram"></i></a>
                        <a href="#" title="Youtube"><i class="fa fa-youtube-play"></i></a>
                    </div>
                    <!--Off Canvas Widget Social End-->
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of offcanvas mobile menu  ====================-->
    <!--====================  hero slider area ====================-->
    <div class="hero-area space__bottom--r120">
        <div class="hero-area-wrapper">
            <div class="single-hero-slider single-hero-slider--background position-relative bg-img" data-bg="assets/img/hero-slider/home2-slider1-image2.webp">
                <!-- hero slider content -->
                <div class="single-hero-slider__abs-img">
                    <img width="977" height="839" src="assets/img/hero-slider/home2-slider1.webp" class="img-fluid" alt="">
                    <img width="978" height="849" src="assets/img/hero-slider/home2-slider2.webp" class="img-fluid" alt="">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="hero-slider-content hero-slider-content--extra-space">
                                <h3 class="hero-slider-content__subtitle hero-slider-content__subtitle hero-slider-content__subtitle--dark">
                                    PT Pioneer CNC Indonesia</h3>
                                <h2 class="hero-slider-content__title hero-slider-content__title--dark space__bottom--50">best quality
                                    for the excellents
                                </h2>
                                <a href="contact.html" class="default-btn default-btn--hero-slider">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of hero slider area  ====================-->
    <!--====================  feature area ====================-->
    <div class="feature-area space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-xl-6 space__bottom__md--40 space__bottom__lm--40">
                    <!-- feature content image -->
                    <div class="feature-content-image">
                        <img width="338" height="367" src="assets/img/feature/feature-banner-3.webp" class="img-fluid" alt="">
                        <img width="301" height="372" src="assets/img/feature/feature-banner-2.webp" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="col-lg-6 col-xl-5 offset-xl-1">
                    <!-- feature content wrapper -->
                    <div class="feature-content-wrapper space__bottom--m40">
                        <div class="single-feature space__bottom--40">
                            <div class="single-feature__icon">
                                <img width="53" height="53" src="assets/img/icons/feature-1.webp" class="img-fluid" alt="">
                            </div>
                            <div class="single-feature__content">
                                <h4 class="single-feature__title">TKDN</h4>
                                <p class="single-feature__text">TKDN (Tingkat Komponen Dalam Negeri) merupakan besarnya komponen dalam negeri pada barang, jasa dan gabungan barang dan jasa.</p>
                            </div>
                        </div>
                        <div class="single-feature space__bottom--40">
                            <div class="single-feature__icon">
                                <img width="53" height="53" src="assets/img/icons/feature-2.webp" class="img-fluid" alt="">
                            </div>
                            <div class="single-feature__content">
                                <h4 class="single-feature__title">Kualitas Terbaik</h4>
                                <p class="single-feature__text">Kami selalu menyediakan produk yang berkualitas dan menggunakan bahan bermutu tinggi untuk para konsumen</p>
                            </div>
                        </div>
                        <div class="single-feature space__bottom--40">
                            <div class="single-feature__icon">
                                <img width="53" height="53" src="assets/img/icons/feature-3.webp" class="img-fluid" alt="">
                            </div>
                            <div class="single-feature__content">
                                <h4 class="single-feature__title">Mesin CNC Fiber Laser</h4>
                                <p class="single-feature__text">Tim kami telah melakukan riset dengan cermat untuk menghadirkan mesin cnc fiber laser sebagai produk unggulan dengan desain yang lebih futuristik dan tentunya sangat mampu digunakan untuk high speed cutting.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of feature area  ====================-->
    <!--====================  about area ====================-->
    <div class="about-area space__bottom--r120 ">
        <div class="container">
            <div class="row align-items-center row-25">
                <div class="col-md-6 order-2 order-md-1">
                    <div class="about-content">
                        <!-- section title -->
                        <div class="section-title space__bottom--25">
                            <h3 class="section-title__sub">Produk Unggulan</h3>
                            <h2 class="section-title__title">Mesin CNC Fiber Laser Cutting</h2>
                        </div>
                        <p class="about-content__text space__bottom--40">Tim kami telah melakukan riset dengan cermat untuk
                            menghadirkan mesin cnc fiber laser generasi ke-2 dengan harga terjangkau untuk umkm.
                            Customer pun bisa mendapatkan mesin cnc fiber laser kami dengan desain yang lebih futuristik
                            dan tentunya sangat mampu digunakan untuk high speed cutting.</p>
                        <a href="contact.html" class="default-btn">Jelajahi Produk Kami</a>
                    </div>
                </div>
                <div class="col-md-6 order-1 order-md-2">
                    <div class="about-image space__bottom__lm--30">
                        <img width="671" height="408" src="assets/img/about/about-section-2.webp" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of about area  ====================-->
    <!--====================  service area ====================-->

    <!--====================  End of service area  ====================-->
    <!--====================  cta area ====================-->
    <div class="cta-area cta-area-bg bg-img" data-bg="assets/img/backgrounds/cta-bg2.webp">
        <div class="cta-wrapper background-color--dark-overlay space__inner__top--50 space__inner__bottom--150">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 mx-auto">
                        <div class="cta-block cta-block--default-color">
                            <p class="cta-block__light-text text-center">Berpengalaman lebih dari <span>15</span> tahun</p>
                            <p class="cta-block__semi-bold-text cta-block__semi-bold-text--medium text-center">Best quality for the excellents </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of cta area  ====================-->
    <!-- funfact include -->
    <div class="funfact-wrapper space__top--m100">
        <!--====================  fun fact area ====================-->
        <div class="fun-fact-area space__bottom--r120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- fun fact wrapper -->
                        <div class="fun-fact-wrapper fun-fact-wrapper-bg bg-img" data-bg="assets/img/backgrounds/funfact-bg.webp">
                            <div class="fun-fact-inner background-color--default-overlay background-repeat--x-bottom space__inner--y30 bg-img" data-bg="assets/img/icons/ruler-black.webp">
                                <div class="fun-fact-content-wrapper">
                                    <div class="single-fun-fact">
                                        <h3 class="single-fun-fact__number counter">985</h3>
                                        <h4 class="single-fun-fact__text">Produk</h4>
                                    </div>
                                    <div class="single-fun-fact">
                                        <h3 class="single-fun-fact__number counter">529</h3>
                                        <h4 class="single-fun-fact__text">Clients</h4>
                                    </div>
                                    <div class="single-fun-fact">
                                        <h3 class="single-fun-fact__number counter">888</h3>
                                        <h4 class="single-fun-fact__text">Success</h4>
                                    </div>
                                    <div class="single-fun-fact">
                                        <h3 class="single-fun-fact__number counter">100</h3>
                                        <h4 class="single-fun-fact__text">Awards</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of fun fact area  ====================-->
    </div>
    <!--====================  project area ====================-->
    <div class="project-area space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- section title -->
                    <div class="section-title text-center  space__bottom--40 mx-auto">
                        <h3 class="section-title__sub">Katalog Produk Kami</h3>
                        <h2 class="section-title__title">Berbagai produk hasil inovasi kami untuk anda</h2>
                    </div>
                </div>
            </div>
            <div class="project-wrapper project-wrapper--masonry row space__bottom--m30">
                <div class="col-1 gutter"></div>
                <div class="col-lg-4 col-md-6 grid-item space__bottom--30">
                    <div class="single-project-wrapper single-project-wrapper--reduced-abs">
                        <a class="single-project-item p-0" href="project-details.html">
                            <img width="370" height="600" src="assets/img/projects/project9.webp" class="img-fluid" alt="">
                            <span class="single-project-title">Mesin CNC Fiber Laser Cutting</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 grid-item space__bottom--30">
                    <div class="single-project-wrapper single-project-wrapper--reduced-abs">
                        <a class="single-project-item p-0" href="project-details.html">
                            <img width="470" height="285" src="assets/img/projects/project10.webp" class="img-fluid" alt="">
                            <span class="single-project-title">Mesin CNC Plasma Cutting</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 grid-item space__bottom--30">
                    <div class="single-project-wrapper single-project-wrapper--reduced-abs">
                        <a class="single-project-item p-0" href="project-details.html">
                            <img width="270" height="285" src="assets/img/projects/project11.webp" class="img-fluid" alt="">
                            <span class="single-project-title">Mesin CNC Router Cutting</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-5 grid-item space__bottom--30">
                    <div class="single-project-wrapper single-project-wrapper--reduced-abs">
                        <a class="single-project-item p-0" href="project-details.html">
                            <img width="270" height="285" src="assets/img/projects/project12.webp" class="img-fluid" alt="">
                            <span class="single-project-title">Mesin CNC Rotary Cutting</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-7 grid-item space__bottom--30">
                    <div class="single-project-wrapper single-project-wrapper--reduced-abs">
                        <a class="single-project-item p-0" href="project-details.html">
                            <img width="470" height="285" src="assets/img/projects/project13.webp" class="img-fluid" alt="">
                            <span class="single-project-title">Mesin CNC Plano Milling</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of project area  ====================-->
    <!--====================  team area ====================-->
    <div class="team-area space__bottom--r120 position-relative">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 space__bottom__md--40 space__bottom__lm--40">
                    <div class="team-member-title-wrapper">
                        <!-- section title -->
                        <div class="section-title space__bottom--30 space__bottom__md--30 space__bottom__lm--20">
                            <h3 class="section-title__sub">Tim Kami</h3>
                            <h2 class="section-title__title">Solid & Terbaik</h2>
                        </div>
                        <p class="team-text space__bottom--40 space__bottom__md--30 space__bottom__lm--20">
                            Kami pun terus berbenah dengan sistem kerja yang kini lebih kondusif dan efisien demi terwujudnya produk mesin terbaik bagi para customer yang luar biasa.</p>
                        <a href="about.html" class="default-btn">View more</a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="row align-items-center">
                        <div class="col-lg-7 col-md-6">
                            <ul class="nav team-member-link-wrapper" id="nav-tab2" role="tablist">

                                <li class="nav-item">
                                    <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#team-member1">
                                        <img width="200" height="190" src="assets/img/team/team-member1-sq.webp" class="img-fluid" alt="">
                                    </button>
                                </li>
                                <li class="nav-item">
                                    <button class="nav-link" data-bs-toggle="tab" data-bs-target="#team-member2">
                                        <img width="200" height="190" src="assets/img/team/team-member2-sq.webp" class="img-fluid" alt="">
                                    </button>
                                </li>
                                <li class="nav-item">
                                    <button class="nav-link" data-bs-toggle="tab" data-bs-target="#team-member3">
                                        <img width="200" height="190" src="assets/img/team/team-member3-sq.webp" class="img-fluid" alt="">
                                    </button>
                                </li>
                                <li class="nav-item">
                                    <button class="nav-link" data-bs-toggle="tab" data-bs-target="#team-member4">
                                        <img width="200" height="190" src="assets/img/team/team-member4-sq.webp" class="img-fluid" alt="">
                                    </button>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-5 col-md-6">
                            <div class="tab-content team-member__content-wrapper">
                                <div class="tab-pane fade show active" id="team-member1" role="tabpanel">
                                    <div class="single-team-member--shadow text-center">
                                        <div class="single-team-member__image">
                                            <img width="510" height="776" src="assets/img/team/team-member1.webp" alt="">
                                        </div>
                                        <div class="single-team-member__content">
                                            <h5 class="single-team-member__name">Syaiful Adhim</h5>
                                            <p class="single-team-member__des">CEO</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="team-member2" role="tabpanel">
                                    <div class="single-team-member--shadow text-center">
                                        <div class="single-team-member__image">
                                            <img width="510" height="775" src="assets/img/team/team-member2.webp" alt="">
                                        </div>
                                        <div class="single-team-member__content">
                                            <h5 class="single-team-member__name">Franky Moina</h5>
                                            <p class="single-team-member__des">Chief Electrician</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="team-member3" role="tabpanel">
                                    <div class="single-team-member--shadow text-center">
                                        <div class="single-team-member__image">
                                            <img width="510" height="775" src="assets/img/team/team-member3.webp" alt="">
                                        </div>
                                        <div class="single-team-member__content">
                                            <h5 class="single-team-member__name">Navira Parey</h5>
                                            <p class="single-team-member__des">Chief Architect</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="team-member4" role="tabpanel">
                                    <div class="single-team-member--shadow text-center">
                                        <div class="single-team-member__image">
                                            <img width="510" height="775" src="assets/img/team/team-member4.webp" alt="">
                                        </div>
                                        <div class="single-team-member__content">
                                            <h5 class="single-team-member__name">Tandur Belali</h5>
                                            <p class="single-team-member__des">Engineer</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of team area  ====================-->
    <!--====================  testimonial area ====================-->
    <div class="testimonial-area space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- section title -->
                    <div class="section-title text-center  space__bottom--40 mx-auto">
                        <h3 class="section-title__sub">Testimoni</h3>
                        <h2 class="section-title__title">Apa pendapat konsumen tentang kami?</h2>
                    </div>
                    <!-- testimonial slider -->
                    <div class="testimonial-multi-slider-wrapper space__inner__bottom--50 space__inner__bottom__md--50 space__inner__bottom__lm--50">
                        <div class="single-testimonial single-testimonial--style2">
                            <p class="single-testimonial__text space__bottom--20"> <span class="quote-left">"</span>
                                Produk PT Pioneer CNC Indonesia memang yang terbaik dan saya puas <span class="quote-right">"</span></p>
                            <h5 class="single-testimonial__author">Nico Vernandes</h5>
                            <p class="single-testimonial__author-des">CEO, Fine Group</p>
                        </div>
                        <div class="single-testimonial single-testimonial--style2">
                            <p class="single-testimonial__text space__bottom--20"> <span class="quote-left">"</span>
                                Pelayanannya bagus, produk jelas berkualitas <span class="quote-right">"</span></p>
                            <h5 class="single-testimonial__author">Muhammad William</h5>
                            <p class="single-testimonial__author-des">CTO, HB Group</p>
                        </div>
                        <div class="single-testimonial single-testimonial--style2">
                            <p class="single-testimonial__text space__bottom--20"> <span class="quote-left">"</span>
                                Produknya the best, layanan after salesnya sangat oke <span class="quote-right">"</span></p>
                            <h5 class="single-testimonial__author">Ramadhani</h5>
                            <p class="single-testimonial__author-des">UD Abadi Makmur</p>
                        </div>
                        <div class="single-testimonial single-testimonial--style2">
                            <p class="single-testimonial__text space__bottom--20"> <span class="quote-left">"</span>
                                Teknologi Mesinnya cangghih, mudah penggunaannya <span class="quote-right">"</span></p>
                            <h5 class="single-testimonial__author">ALi Abdullah</h5>
                            <p class="single-testimonial__author-des">CV Pagar Abadi</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of testimonial area  ====================-->
    <!--====================  blog grid slider area ====================-->

    <!--====================  End of blog grid slider area  ====================-->
    <!--====================  brand logo area ====================-->
    <div class="brand-logo-area space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- brand logo slider -->
                    <div class="brand-logo-wrapper">
                        <div class="single-brand-logo">
                            <a href="#"><img width="126" height="50" src="assets/img/brand-logo/logo1.webp" class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img width="129" height="49" src="assets/img/brand-logo/logo2.webp" class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img width="113" height="49" src="assets/img/brand-logo/logo3.webp" class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img width="141" height="55" src="assets/img/brand-logo/logo4.webp" class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img width="146" height="54" src="assets/img/brand-logo/logo5.webp" class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img width="141" height="55" src="assets/img/brand-logo/logo4.webp" class="img-fluid" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of brand logo area  ====================-->
    <!--====================  newsletter area ====================-->
    <div class="newsletter-area newsletter-area-bg bg-img" data-bg="assets/img/backgrounds/newsletter-bg.webp">
        <div class="newsletter-wrapper background-color--default-overlay space__inner--y60">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-xl-10 mx-auto">
                        <div class="row align-items-center">
                            <div class="col-lg-4 mb-3 mb-lg-0">
                                <!-- newsletter title -->
                                <h3 class="newsletter-title"><span>Best quality</span> for the excellents</h3>
                            </div>
                            <div class="col-lg-8">

                                    <!-- mailchimp-alerts Start -->

                                    <!-- mailchimp-alerts end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of newsletter area  ====================-->
    <!--====================  footer area ====================-->
    <div class="footer-area bg-img space__inner--ry120" data-bg="assets/img/backgrounds/footer-bg.webp">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="footer-widget">
                        <div class="footer-widget__logo space__bottom--40">
                            <a href="index.html">
                                <img width="142" height="31" src="assets/img/logo-white.webp" class="img-fluid" alt="">
                            </a>
                        </div>
                        <p class="footer-widget__text space__bottom--20">
                            PT Pioneer CNC Indonesia
                            Pelopor manufaktur mesin CNC
                            Best Quality for The Excellents</p>
                        <ul class="social-icons">
                            <li><a href="//www.facebook.com/people/PT-Pioneer-CNC-Indonesia/100090826561438/?mibextid=ZbWKwL"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="//www.youtube.com/@OfficialPT.PioneerCNCIndonesia"><i class="fa fa-youtube-play"></i></a></li>
                            <li><a href="//www.instagram.com/ptpioneercncindonesia.official/"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="//mail.google.com/mail/u/0/?view=cm&tf=1&fs=1&to=pt.pioneercncindonesiaofficial@gmail.com"><i class="fa fa-envelope"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="footer-widget space__top--15 space__top__md--40 space__top__lm--40">

                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget space__top--15 space__top__md--40 space__top__lm--40">

                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h5 class="footer-widget__title space__top--15 space__bottom--20 space__top__md--40 space__top__lm--40">
                        Contact us</h5>
                    <div class="footer-contact-wrapper">
                        <div class="single-footer-contact">
                            <div class="single-footer-contact__icon"><i class="fa fa-map-marker"></i></div>
                            <div class="single-footer-contact__text">Jl. Raya Kedungrejo Genitri No.86, RT.001/RW.001, Genitri, Kedungrejo, Kec. Pakis, Kabupaten Malang, Jawa Timur 65154</div>
                        </div>
                        <div class="single-footer-contact">
                            <div class="single-footer-contact__icon"><i class="fa fa-whatsapp"></i></div>
                            <div class="single-footer-contact__text"> <a href="https://api.whatsapp.com/send?phone=6282234936001">+6282234936001</a> <br> </div>
                        </div>
                        <div class="single-footer-contact">
                            <div class="single-footer-contact__icon"><i class="fa fa-globe"></i></div>
                            <div class="single-footer-contact__text"><a href="http://www.ptpioneercncindonesia.com">www.ptpioneercncindonesia.com</a>
                                <br>  </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- copyright text -->
    <div class="copyright-area background-color--deep-dark space__inner--y30">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p class="copyright-text">&copy; 2023 <i class="fa fa-chevron-circle-right"></i> by <a href="http://www.ptpioneercncindonesia.com" target="_blank">PT Pioneer CNC Indonesia</a></p>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of footer area  ====================-->
    <!--====================  scroll top ====================-->
    <button class="scroll-top" id="scroll-top">
        <i class="fa fa-angle-up"></i>
    </button>
    <!--====================  End of scroll top  ====================-->
    <!-- JS
    ============================================ -->
    <!-- Modernizer JS -->
    <script src="assets/js/modernizr-2.8.3.min.js"></script>
    <!-- jQuery JS -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Slick slider JS -->
    <script src="assets/js/plugins/slick.min.js"></script>
    <!-- Counterup JS -->
    <script src="assets/js/plugins/counterup.min.js"></script>
    <!-- Waypoint JS -->
    <script src="assets/js/plugins/waypoint.min.js"></script>
    <!-- Justified Gallery JS -->
    <script src="assets/js/plugins/justifiedGallery.min.js"></script>
    <!-- Image Loaded JS -->
    <script src="assets/js/plugins/imageloaded.min.js"></script>
    <!-- Maosnry JS -->
    <script src="assets/js/plugins/masonry.min.js"></script>
    <!-- Light Gallery JS -->
    <script src="assets/js/plugins/light-gallery.min.js"></script>
    <!-- Mailchimp JS -->
    <script src="assets/js/plugins/mailchimp-ajax-submit.min.js"></script>
    <!-- Plugins JS (Please remove the comment from below plugins.min.js for better website load performance and remove plugin js files from avobe) -->
    <!--
    <script src="assets/js/plugins/plugins.min.js"></script>
    -->
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>
</body>
</html>
