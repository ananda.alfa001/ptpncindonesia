<!-- JS
    ============================================ -->
    <!-- Modernizer JS -->
    <script src="{{asset('assets/js/modernizr-2.8.3.min.js')}}"></script>
    <!-- jQuery JS -->
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <!-- Popper JS -->
    <script src="{{asset('assets/js/popper.min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- Slick slider JS -->
    <script src="{{asset('assets/js/plugins/slick.min.js')}}"></script>
    <!-- Counterup JS -->
    <script src="{{asset('assets/js/plugins/counterup.min.js')}}"></script>
    <!-- Waypoint JS -->
    <script src="{{asset('assets/js/plugins/waypoint.min.js')}}"></script>
    <!-- Justified Gallery JS -->
    <script src="{{asset('assets/js/plugins/justifiedGallery.min.js')}}"></script>
    <!-- Image Loaded JS -->
    <script src="{{asset('assets/js/plugins/imageloaded.min.js')}}"></script>
    <!-- Maosnry JS -->
    <script src="{{asset('assets/js/plugins/masonry.min.js')}}"></script>
    <!-- Light Gallery JS -->
    <script src="{{asset('assets/js/plugins/light-gallery.min.js')}}"></script>
    <!-- Mailchimp JS -->
    <script src="{{asset('assets/js/plugins/mailchimp-ajax-submit.min.js')}}"></script>
    <!-- Plugins JS (Please remove the comment from below plugins.min.js for better website load performance and remove plugin js files from avobe) -->
    <!--
    <script src="assets/js/plugins/plugins.min.js"></script>
    -->
    <!-- Main JS -->
    <script src="{{asset('assets/js/main.js')}}"></script>
