         <!--====================  header area ====================-->
         <div class="header-area header-sticky bg-img space__inner--y40 background-repeat--x background-color--dark d-none d-lg-block"
             data-bg="assets/img/icons/ruler.webp">
             <!-- header top -->
             <div class="header-top">
                 <div class="container">
                     <div class="row">
                         <div class="col-lg-4">
                             <div class="header-top-info">
                                 <span class="header-top-info__image pr-1"><img width="20" height="19"
                                         src="assets/img/icons/phone.webp" alt=""></span>
                                 <span class="header-top-info__text">0822-3493-6001</span>
                             </div>
                         </div>
                         <div class="col-lg-4">

                         </div>
                         <div class="col-lg-4">

                         </div>
                     </div>
                 </div>
             </div>
             <!-- menu bar -->
             <div class="menu-bar">
                 <div class="container">
                     <div class="row">
                         <div class="col-lg-12  position-relative">
                             <div class="menu-bar-wrapper background-color--default space__inner--x35">
                                 <div class="menu-bar-wrapper-inner">
                                     <div class="row align-items-center">
                                         <div class="col-lg-2">
                                             <div class="brand-logo">
                                                 <a href="{{route('home')}}">
                                                     <img width="142" height="31" src="assets/img/logo.webp"
                                                         class="img-fluid" alt="">
                                                 </a>
                                             </div>
                                         </div>
                                         <div class="col-lg-10">
                                             <div class="navigation-area d-flex justify-content-end align-items-center">
                                                 <!-- navigation menu -->
                                                 <nav class="main-nav-menu">
                                                     <ul class="d-flex justify-content-end">
                                                         <li>
                                                             <a href="{{route('home')}}">Home</a>
                                                         </li>
                                                         <li class="has-sub-menu">
                                                             <a href="#">About</a>
                                                             <ul class="sub-menu">
                                                                 <li><a href="{{route('about.visimisi')}}">Visi & Misi</a></li>
                                                                 <li><a href="{{route('about.struktur')}}">Struktur Organisasi</a></li>                                                  
                                                             </ul>
                                                         </li>

                                                         <li class="has-sub-menu">
                                                             <a href="#">Produk</a>
                                                             <ul class="sub-menu">
                                                                <li class="has-sub-menu">
                                                                            <a href="#">Mesin CNC Fiber Laser Cutting</a>
                                                                            <ul class="sub-menu">                                                                           
                                                                                <a href="{{route('product.fiberlasercutting1')}}">CNC CUTTING FIBER LASER GEN 1</a>                                                                               
                                                                                </li>
                                                                                <li><a href="{{route('product.fiberlasercutting2')}}">CNC CUTTING FIBER LASER GEN 2</a></li>
                                                                            </ul>
                                                                        </li>                                                       
                                                                        </li>

                                                                        <li class="has-sub-menu">
                                                                            <a href="#">Mesin CNC Plasma Cutting</a>
                                                                            <ul class="sub-menu">                                                                           
                                                                                <a href="{{route('product.plasmacutting1')}}">CNC PLASMA CUTTING GEN 1</a>                                                                               
                                                                                </li>
                                                                                <li><a href="{{route('product.plasmacutting2')}}">CNC PLASMA CUTTING GEN 2</a></li>
                                                                            </ul>
                                                                        </li>                                                       
                                                                        </li>
                                                                 
                                                                        <li class="has-sub-menu">
                                                                            <a href="#">Mesin CNC Router</a>
                                                                            <ul class="sub-menu"> 
                                                                                <a href="{{route('product.router1')}}">CNC ROUTER</a>                                                                               
                                                                                </li>
                                                                                <li><a href="{{route('product.routeratcmultispindle')}}">CNC ROUTER ATC MULTISPINDLE</a>
                                                                                </li>
                                                                                <li><a href="{{route('product.routeratc')}}">CNC ROUTER ATC</a>
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                 <li><a href="{{route('product.plasmarotarycutting')}}">Mesin CNC Rotary Cutting</a>
                                                                 </li>
                                                                 <li><a href="#">Mesin CNC Plano Milling</a>
                                                                 </li>

                                                             </ul>
                                                         </li>
                                                         </li>
                                                         <li><a href="{{ route('home.contact') }}">Contact</a></li>
                                                     </ul>
                                                 </nav>
                                                 <!-- search icon nav menu -->
                                                 <div class="nav-search-icon">
                                                     <button class="header-search-toggle"><i
                                                             class="fa fa-search"></i></button>
                                                     <div class="header-search-form">
                                                         <form action="#">
                                                             <input type="text" placeholder="Type and hit enter">
                                                             <button><i class="fa fa-search"></i></button>
                                                         </form>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <!--====================  End of header area  ====================-->
         <!--====================  mobile header ====================-->
         <div class="mobile-header header-sticky bg-img space__inner--y30 background-repeat--x background-color--dark d-block d-lg-none"
             data-bg="assets/img/icons/ruler.webp">
             <div class="container">
                 <div class="row align-items-center">
                     <div class="col-6">
                         <div class="brand-logo">
                             <a href="{{route('home')}}">
                                 <img width="142" height="31" src="assets/img/logo-white.webp" class="img-fluid"
                                     alt="">
                             </a>
                         </div>
                     </div>
                     <div class="col-6">
                         <div class="mobile-menu-trigger-wrapper text-end" id="mobile-menu-trigger">
                             <span class="mobile-menu-trigger"></span>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <!--====================  End of mobile header  ====================-->
         <!--====================  offcanvas mobile menu ====================-->
         <div class="offcanvas-mobile-menu" id="mobile-menu-overlay">
             <a href="javascript:void(0)" class="offcanvas-menu-close" id="mobile-menu-close-trigger">
                 <span class="menu-close"></span>
             </a>
             <div class="offcanvas-wrapper">
                 <div class="offcanvas-inner-content">
                     <div class="offcanvas-mobile-search-area">
                         <form action="#">
                             <input type="search" placeholder="Search ...">
                             <button type="submit"><i class="fa fa-search"></i></button>
                         </form>
                     </div>
                     <nav class="offcanvas-navigation">
                         <ul>

                             <li><a href="{{ route('home') }}">Home</a></li>
                             <li class="menu-item-has-children">
                                <a href="javascript:void(0)">Company</a>
                                <ul class="sub-menu-mobile">
                                    <li><a href="{{ route('about.visimisi') }}">Visi & Misi</a></li>
                                    <li><a href="{{ route('about.struktur') }}">Struktur Organisasi</a></li>
                                </ul>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="javascript:void(0)">Produk</a>
                                <ul class="sub-menu-mobile">
                                    
                                    <li class="menu-item-has-children">
                                        <a href="#"> CNC Fiber Laser Cutting</a>
                                        <ul class="sub-menu-mobile">
                                            <li class="menu-item-has-children">
                                                <a href="{{route('product.fiberlasercutting1')}}">CNC CUTTING FIBER LASER GEN 1</a>
                    
                                            </li>
                                            <li><a href="{{route('product.fiberlasercutting2')}}">CNC CUTTING FIBER LASER GEN 2</a></li>
                                        </ul>
                                    </li>

                                    <li class="menu-item-has-children">
                                        <a href="#"> Mesin CNC Plasma Cutting</a>
                                        <ul class="sub-menu-mobile">
                                            <li class="menu-item-has-children">
                                                <a href="{{route('product.plasmacutting1')}}">CNC PLASMA CUTTING GEN 1</a>
                    
                                            </li>
                                            <li><a href="{{route('product.plasmacutting2')}}">CNC PLASMA CUTTING GEN 2</a></li>
                                        </ul>
                                    </li>
                                    
                                    <li class="menu-item-has-children">
                                        <a href="#">Mesin CNC Router</a>
                                        <ul class="sub-menu-mobile">
                                            <li class="menu-item-has-children">
                                                <a href="{{route('product.router1')}}">CNC ROUTER</a>
                    
                                            </li>
                                            <li><a href="{{route('product.routeratcmultispindle')}}">CNC ROUTER ATC MULTISPINDLE</a></li>
                                            <li><a href="{{route('product.routeratc')}}">CNC ROUTER ATC</a></li>
                                        </ul>
                                    </li>

                                    <li class="menu-item-has-children">
                                        <a href="#">Mesin CNC Rotary Cutting </a>
                                        <ul class="sub-menu-mobile">
                                            <li class="menu-item-has-children">
                                                <a href="{{route('product.plasmarotarycutting')}}">PLASMA ROTARY CUTTING</a>
                    
                                            </li>
                                            
                                        </ul>
                                    </li>
                                    
                                
                                    
                                    <li><a href="#">Mesin CNC Plano Milling</a></li>
                                </ul>
                            </li>
                             <li><a href="{{ route('home.contact') }}">Contact</a></li>
                         </ul>
                     </nav>
                     <div class="offcanvas-widget-area">
                         <div class="off-canvas-contact-widget">
                             <div class="header-contact-info">
                                 <ul class="header-contact-info__list">
                                     <li><i class="fa fa-phone"></i> 0822-3493-6001</li>
                                     <li><i class="fa fa-calendar"></i> Senin - Sabtu</li>
                                     <br>
                                     <li><i class="fa fa-clock-o"></i> 8.00 - 16.00</li>
                                 </ul>
                             </div>
                         </div>
                         <!--Off Canvas Widget Social Start-->
                         <div class="off-canvas-widget-social">
                             <a href="//www.facebook.com/people/PT-Pioneer-CNC-Indonesia/100090826561438/?mibextid=ZbWKwL" title="Facebook"><i class="fa fa-facebook"></i></a>
                             <a href="//www.instagram.com/ptpioneercncindonesia.official" title="Instagram"><i class="fa fa-instagram"></i></a>
                             <a href="//www.youtube.com/@OfficialPT.PioneerCNCIndonesia" title="Youtube"><i class="fa fa-youtube-play"></i></a>
                         </div>
                         <!--Off Canvas Widget Social End-->
                     </div>
                 </div>
             </div>
         </div>
         <!--====================  End of offcanvas mobile menu  ====================-->
