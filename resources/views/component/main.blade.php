<!DOCTYPE html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PT PIONEER CNC INDONESIA</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Global stylesheets -->
    @include('component.style')
    <!-- /global stylesheets -->
</head>
<body>
    @include('component.navbar')
    <main id="main">
        {{-- =========================== --}}
        @yield('content')
        {{-- =========================== --}}
    </main>
    <!-- ======= Footer ======= -->
    <footer id="footer">
        @include('component.footer')
    </footer>
    <!--====================  scroll top ====================-->
    <button class="scroll-top" id="scroll-top">
        <i class="fa fa-angle-up"></i>
    </button>
    <!--====================  End of scroll top  ====================-->
    @include('component.script')
</body>

</html>
