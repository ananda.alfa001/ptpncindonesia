
    <!--====================  footer area ====================-->
    <div class="footer-area bg-img space__inner--ry120" data-bg="assets/img/backgrounds/footer-bg.webp">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="footer-widget">
                        <div class="footer-widget__logo space__bottom--40">
                            <a href="index.html">
                                <img width="142" height="31" src="assets/img/logo-white.webp" class="img-fluid" alt="">
                            </a>
                        </div>
                        <p class="footer-widget__text space__bottom--20">
                            PT Pioneer CNC Indonesia
                            <br>Pelopor manufaktur mesin CNC
                            <br>Best Quality for The Excellents</p>
                        <ul class="social-icons">
                            <li><a href="//www.facebook.com/people/PT-Pioneer-CNC-Indonesia/100090826561438/?mibextid=ZbWKwL"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="//www.youtube.com/@OfficialPT.PioneerCNCIndonesia"><i class="fa fa-youtube-play"></i></a></li>
                            <li><a href="//www.instagram.com/ptpioneercncindonesia.official/"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="//mail.google.com/mail/u/0/?view=cm&tf=1&fs=1&to=pt.pioneercncindonesiaofficial@gmail.com"><i class="fa fa-envelope"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6">
                    <div class="footer-widget space__top--15 space__top__md--40 space__top__lm--40">

                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget space__top--15 space__top__md--40 space__top__lm--40">

                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h5 class="footer-widget__title space__top--15 space__bottom--20 space__top__md--40 space__top__lm--40">
                        Contact us</h5>
                    <div class="footer-contact-wrapper">
                        <div class="single-footer-contact">
                            <div class="single-footer-contact__icon"><i class="fa fa-map-marker"></i></div>
                            <div class="single-footer-contact__text">Jl. Raya Kedungrejo Genitri No.86, RT.001/RW.001, Genitri, Kedungrejo, Kec. Pakis, Kabupaten Malang, Jawa Timur 65154</div>
                        </div>
                        <div class="single-footer-contact">
                            <div class="single-footer-contact__icon"><i class="fa fa-whatsapp"></i></div>
                            <div class="single-footer-contact__text"> <a href="https://api.whatsapp.com/send?phone=6282234936001">+6282234936001</a> <br> </div>
                        </div>
                        <div class="single-footer-contact">
                            <div class="single-footer-contact__icon"><i class="fa fa-globe"></i></div>
                            <div class="single-footer-contact__text"><a href="https://www.ptpioneercncindonesia.com">www.ptpioneercncindonesia.com</a>
                                <br>  </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- copyright text -->
    <div class="copyright-area background-color--deep-dark space__inner--y30">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p class="copyright-text">&copy; 2023 <i class="fa fa-chevron-circle-right"></i> by <a href="https://www.ptpioneercncindonesia.com" target="_blank">PT Pioneer CNC Indonesia</a></p>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of footer area  ====================-->

