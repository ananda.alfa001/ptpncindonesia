<!-- Favicon -->
<link rel="icon" href="{{asset('assets/img/favicon.ico')}}">
<!-- CSS
    ============================================ -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@300;400;500;600;700&display=swap" rel="stylesheet">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
<!-- FontAwesome CSS -->
<link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
<!-- Flaticon CSS -->
<link rel="stylesheet" href="{{asset('assets/css/flaticon.min.css')}}">
<!-- Slick Slider CSS -->
<link rel="stylesheet" href="{{asset('assets/css/plugins/slick.min.css')}}">
<!-- CSS Animation CSS -->
<link rel="stylesheet" href="{{asset('assets/css/plugins/cssanimation.min.css')}}">
<!-- Justified Gallery CSS -->
<link rel="stylesheet" href="{{asset('assets/css/plugins/justifiedGallery.min.css')}}">
<!-- Light Gallery CSS -->
<link rel="stylesheet" href="{{asset('assets/css/plugins/light-gallery.min.css')}}">
<!-- Vendor & Plugins CSS (Please remove the comment from below vendor.min.css & plugins.min.css for better website load performance and remove css files from avobe) -->
<!--
    <link rel="stylesheet" href="assets/css/vendor.min.css">
    <link rel="stylesheet" href="assets/css/plugins/plugins.min.css">
    -->
<!-- Main Style CSS -->
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
