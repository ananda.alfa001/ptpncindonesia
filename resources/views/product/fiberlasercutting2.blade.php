@extends('component.main')
@section('content')
    <!--box atas-->
    <div class="page-breadcrumb bg-img space__bottom--r120" data-bg="assets/img/backgrounds/bc-bg.webp">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="page-breadcrumb-content text-center">
                        <h1>CNC CUTTING FIBER LASER GEN 2</h1>
                        <ul class="page-breadcrumb-links">
                            <li><a href="{{ route('home') }}">Home</a></li>
                            <li>Product Details</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end box atas-->
    <!--====================  project details area ====================-->
    <div class="project-section space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-12 space__bottom--40">
                    <div class="project-image"><img width="1170" height="439" src="assets/img/projects/cnc-cutting-fiber-laser-gen2.webp" class="img-fluid" alt=""></div>
                </div>
                <div class="col-lg-4 col-12 space__bottom--30">
                    <div class="project-information">
                        <h4 class="space__bottom--15">Tipe Produk</h4>
                        <h3 class="space__bottom--15">FL-C1500 GEN 2</h3>
                        <ul>
                            <li><strong>Tahun Produksi:</strong> <a href="#">2022</a></li>
                            <li><strong>Material Potong:</strong> All Metal</li>
                            <li><strong>Kategori:</strong> <a href="#">Mesin CNC Fiber Laser Cutting</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-12 space__bottom--30">
                    <div class="project-details">
                        <h3 class="space__bottom--15">CNC CUTTING FIBER LASER GEN 2</h3>
                        <p>Mesin yang berfungsi untuk proses pemotongan sheet metal dengan berbagai motif. Mesin ini menggunakan kombinasi sistem komputerisasi dengan hi-tech laser fiber yang merupakan teknologi terbaru untuk pemotongan metal saat ini. Mesin ini mampu memotong material dengan high speed cutting.</p>
                        <br>
                        <strong>Spesifikasi Produk: </strong><br>
                        <li><strong></strong> Raycus Laser Source 1000 Watt</li>
                        <li><strong></strong> Raytools Head BT 240S (Upgradable Up TO 3000W)</li>
                        <li><strong></strong> FS CUT 1000S Controller</li>
                        <li><strong></strong> Panasonic Japan Servo Motor 750W</li>
                        <li><strong></strong> Capacitive Sensor Material (BCS100)</li>
                        <li><strong></strong> Solid Frame by Machining Process</li>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row row-5 image-popup">
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-cutting-fiber-laser-gen2-m1.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-cutting-fiber-laser-gen2-m1.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-cutting-fiber-laser-gen2-m2.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-cutting-fiber-laser-gen2-m2.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-cutting-fiber-laser-gen2-m3.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-cutting-fiber-laser-gen2-m3.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-cutting-fiber-laser-gen2-m4.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-cutting-fiber-laser-gen2-m4.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of project details area  ====================-->
@endsection
