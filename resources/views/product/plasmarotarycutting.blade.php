@extends('component.main')
@section('content')
    <!--box atas-->
    <div class="page-breadcrumb bg-img space__bottom--r120" data-bg="assets/img/backgrounds/bc-bg.webp">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="page-breadcrumb-content text-center">
                        <h1>CNC PLASMA ROTARY CUTTING</h1>
                        <ul class="page-breadcrumb-links">
                            <li><a href="{{ route('home') }}">Home</a></li>
                            <li>Product Details</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end box atas-->
    <!--====================  project details area ====================-->
    <div class="project-section space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-12 space__bottom--40">
                    <div class="project-image"><img width="1170" height="439" src="assets/img/projects/cnc-rotary-plasma-cutting.webp" class="img-fluid" alt=""></div>
                </div>
                <div class="col-lg-4 col-12 space__bottom--30">
                    <div class="project-information">
                        <h4 class="space__bottom--15">Tipe Produk</h4>
                        <h3 class="space__bottom--15">TC-1760P</h3>
                        <ul>
                            <li><strong>Tahun Produksi:</strong> <a href="#">2022</a></li>
                            <li><strong>Material Potong:</strong><br> ACP, Alumunium, Metal </li>
                            <li><strong>Kategori:</strong> <a href="#">CNC ROTARY</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-12 space__bottom--30">
                    <div class="project-details">
                        <h3 class="space__bottom--15">CNC PLASMA ROTARY CUTTING</h3>
                        <p>Mesin cutting untuk tube/square metal dengan menggunakan rotary axis dan plasma. Mesin ini menggunakan teknologi komputerisasi untuk mempermudah pemotongan pipa seperti pipa hollow yang mungkin sebelumnya dilakukan dengan cara manual.</p>
                        <br>
                        <strong>Spesifikasi Produk: </strong><br>
                        <li><strong></strong> Hybrid closed loop motor 12Nm 220VAC 2500rpm, Stepper Motor 4Nm. & 6Nm.</li>
                        <li><strong></strong> 170mm Chuck Holder</li>
                        <li><strong></strong> 100A Digital Plasma Cutting</li>
                        <li><strong></strong> P80 Straight Torch (CNC Dedicated) / Customized Torch</li>
                        <li><strong></strong> Linux CNC Controller</li>
                        <li><strong></strong> Touch Screen Interface Controller</li>
                        <li><strong></strong> Tube and Square Material</li>
                        <li><strong></strong> Solid Frame by Machining Process</li>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row row-5 image-popup">
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-rotary-plasma-cutting-m1.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-rotary-plasma-cutting-m1.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-rotary-plasma-cutting-m2.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-rotary-plasma-cutting-m2.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-rotary-plasma-cutting-m3.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-rotary-plasma-cutting-m3.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-rotary-plasma-cutting-m4.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-rotary-plasma-cutting-m4.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of project details area  ====================-->
@endsection
