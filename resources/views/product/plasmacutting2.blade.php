@extends('component.main')
@section('content')
    <!--box atas-->
    <div class="page-breadcrumb bg-img space__bottom--r120" data-bg="assets/img/backgrounds/bc-bg.webp">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="page-breadcrumb-content text-center">
                        <h1>CNC PLASMA CUTTING GEN 2</h1>
                        <ul class="page-breadcrumb-links">
                            <li><a href="{{ route('home') }}">Home</a></li>
                            <li>Product Details</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end box atas-->
    <!--====================  project details area ====================-->
    <div class="project-section space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-12 space__bottom--40">
                    <div class="project-image"><img width="1170" height="439" src="assets/img/projects/cnc-plasma-cutting-gen2.webp" class="img-fluid" alt=""></div>
                </div>
                <div class="col-lg-4 col-12 space__bottom--30">
                    <div class="project-information">
                        <h4 class="space__bottom--15">Tipe Produk</h4>
                        <h3 class="space__bottom--15">MQ-1325 GEN 2</h3>
                        <ul>
                            <li><strong>Tahun Produksi:</strong> <a href="#">2023</a></li>
                            <li><strong>Material Potong:</strong> All Metal</li>
                            <li><strong>Kategori:</strong> <a href="#">Mesin CNC Plasma Cutting</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-12 space__bottom--30">
                    <div class="project-details">
                        <h3 class="space__bottom--15">CNC PLASMA CUTTING GEN 2</h3>
                        <p>Mesin yang digunakan untuk proses pemotongan sheet metal dengan berbagai motif. Mesin ini menggunakan kombinasi sistem komputerisasi serta menggunakan teknologi plasma cutting (tersedia juga untuk versi gas cutting / blander).</p>
                        <br>
                        <strong>Spesifikasi Produk: </strong><br>
                        <li><strong></strong> 100A Digital Plasma Cutting</li>
                        <li><strong></strong> Touch Screen Interface Controller</li>
                        <li><strong></strong> P80 Straight Torch (CNC dedicated) / Customized Torch</li>
                        <li><strong></strong> Hybrid closed loop motor 12Nm 220VAC 2500rpm, Stepper Motor 4Nm. & 6Nm.</li>
                        <li><strong></strong> THC (Torch Height Control)</li>
                        <li><strong></strong> MACH3 Controller</li>
                        <li><strong></strong> Solid Frame by Machining Process</li>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row row-5 image-popup">
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-plasma-cutting-gen2-m1.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-plasma-cutting-gen2-m1.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-plasma-cutting-gen2-m2.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-plasma-cutting-gen2-m2.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-plasma-cutting-gen2-m3.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-plasma-cutting-gen2-m3.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-plasma-cutting-gen2-m4.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-plasma-cutting-gen2-m4.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of project details area  ====================-->
@endsection
