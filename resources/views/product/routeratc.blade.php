@extends('component.main')
@section('content')
    <!--box atas-->
    <div class="page-breadcrumb bg-img space__bottom--r120" data-bg="assets/img/backgrounds/bc-bg.webp">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="page-breadcrumb-content text-center">
                        <h1>CNC ROUTER ATC</h1>
                        <ul class="page-breadcrumb-links">
                            <li><a href="{{ route('home') }}">Home</a></li>
                            <li>Product Details</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end box atas-->
    <!--====================  project details area ====================-->
    <div class="project-section space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-12 space__bottom--40">
                    <div class="project-image"><img width="1170" height="439" src="assets/img/projects/cnc-router-atc.webp" class="img-fluid" alt=""></div>
                </div>
                <div class="col-lg-4 col-12 space__bottom--30">
                    <div class="project-information">
                        <h4 class="space__bottom--15">Tipe Produk</h4>
                        <h3 class="space__bottom--15">R-ATC 10T</h3>
                        <ul>
                            <li><strong>Tahun Produksi:</strong> <a href="#">2023</a></li>
                            <li><strong>Material Potong:</strong><br> MDF Board, Triplek, Kayu, PVC, ACP </li>
                            <li><strong>Kategori:</strong> <a href="#">CNC ROUTER</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-12 space__bottom--30">
                    <div class="project-details">
                        <h3 class="space__bottom--15">CNC ROUTER ATC</h3>
                        <p>Mesin cnc router ATC menggunakan teknologi komputerisasi dengan 1 spindle untuk pemotongan/ukir bahan metal serta non metal. Mesin router ini memiliki 10 tool ATC (Automatic Tool Changer) yang memiliki fungsi berbeda dalam satu kali proses pengoperasian.</p>
                        <br>
                        <strong>Spesifikasi Produk: </strong><br>
                        <li><strong></strong> Hybrid closed loop motor 12Nm 220VAC 2500rpm, Stepper Motor 4Nm. & 6Nm.</li>
                        <li><strong></strong> 10 tools ATC (Linear Type)</li>
                        <li><strong></strong> Water Cooling Spindle ATC 3,5KW</li>
                        <li><strong></strong> BT30 Tool Holder</li>
                        <li><strong></strong> Linux CNC Controller</li>
                        <li><strong></strong> Touch Screen Interface Controller</li>
                        <li><strong></strong> Solid Frame by Machining Process</li>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row row-5 image-popup">
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-router-atc-m1.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-router-atc-m1.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-router-atc-m2.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-router-atc-m2.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-router-atc-m3.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-router-atc-m3.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-router-atc-m4.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-router-atc-m4.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of project details area  ====================-->
@endsection
