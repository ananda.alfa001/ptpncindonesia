@extends('component.main')
@section('content')
    <!--box atas-->
    <div class="page-breadcrumb bg-img space__bottom--r120" data-bg="assets/img/backgrounds/bc-bg.webp">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="page-breadcrumb-content text-center">
                        <h1>CNC ROUTER</h1>
                        <ul class="page-breadcrumb-links">
                            <li><a href="{{ route('home') }}">Home</a></li>
                            <li>Product Details</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end box atas-->
    <!--====================  project details area ====================-->
    <div class="project-section space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-12 space__bottom--40">
                    <div class="project-image"><img width="1170" height="439" src="assets/img/projects/cnc-router-R-1325.webp" class="img-fluid" alt=""></div>
                </div>
                <div class="col-lg-4 col-12 space__bottom--30">
                    <div class="project-information">
                        <h4 class="space__bottom--15">Tipe Produk</h4>
                        <h3 class="space__bottom--15">R-1325</h3>
                        <ul>
                            <li><strong>Tahun Produksi:</strong> <a href="#">2015</a></li>
                            <li><strong>Material Potong:</strong><br> Alumunium, MDF Board, Triplek, Kayu, PVC </li>
                            <li><strong>Kategori:</strong> <a href="#">Mesin CNC ROUTER</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-12 space__bottom--30">
                    <div class="project-details">
                        <h3 class="space__bottom--15">CNC ROUTER</h3>
                        <p>Mesin CNC router menggunakan teknologi komputerisasi untuk pemotongan dan ukir bahan metal serta non metal. mesin ini menggunakan spindle untuk potong ataupun ukir material.</p>
                        <br>
                        <strong>Spesifikasi Produk: </strong><br>
                        <li><strong></strong> Area kerja 1300mm x 2500mm</li>
                        <li><strong></strong> Hybrid closed loop motor 12Nm 220VAC 2500rpm, Stepper Motor 4Nm. & 6Nm.</li>
                        <li><strong></strong> MACH3 Controller</li>
                        <li><strong></strong> Water Cooling Spindle 2,2KW</li>
                    </div>
                </div>
                <div class="col-12">
                    <div class="row row-5 image-popup">
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-router-R-1325-m1.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-router-R-1325-m1.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-router-R-1325-m2.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-router-R-1325-m2.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-router-R-1325-m3.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-router-R-1325-m3.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-sm-6 col-12 space__top--10">
                            <a href="assets/img/projects/cnc-router-R-1325-m4.webp" class="gallery-item single-gallery-thumb"><img width="440" height="360" src="assets/img/projects/cnc-router-R-1325-m4.webp" class="img-fluid" alt=""><span class="plus"></span></a>
                        </div>
        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of project details area  ====================-->
@endsection
