@extends('component.main')
@section('content')
<!--====================  breadcrumb area ====================-->
<div class="page-breadcrumb bg-img space__bottom--r120" data-bg="assets/img/backgrounds/bc-bg.webp">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="page-breadcrumb-content text-center">
                    <h1>About Us</h1>
                    <ul class="page-breadcrumb-links">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li>Visi Misi</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====================  End of breadcrumb area  ====================-->
<!--====================  about area ====================-->
<div class="about-area space__bottom--r120 ">
    <div class="container">
        <div class="row align-items-center row-25">
            <div class="col-md-6 order-2 order-md-1">
                <div class="about-content">
                    <!-- section title -->
                    <div class="section-title space__bottom--25">
                        <h3 class="section-title__sub">Selayang Pandang</h3>
                        <h2 class="section-title__title">Profil PT Pioneer CNC Indonesia</h2>
                    </div>
                    <p class="about-content__text space__bottom--40">PT Pioneer CNC Indonesia adalah perusahaan yang bergerak di bidang manufaktur Mesin CNC (Computer Numerical Control) seperti Mesin CNC Cutting Fiber Laser, Mesin CNC Router, Mesin CNC Plasma Cutting, Jasa Cutting Laser yang berlokasi di Kabupaten Malang.
                        Sejak tahun 2015 kami aktif melayani banyak customer dari berbagai wilayah di Indonesia dari Sabang sampai Merauke. Kami selalu berusaha memberikan kualitas terbaik dalam produksi mesin untuk kepuasan pelanggan.
                        Tim dan staf kami merupakan satu kekuatan dan kemampuan yang ditopang oleh kedisiplinan ilmu dan keahlian di bidangnya masing-masing.
                        Prinsip perusahaan kami dalam melayani konsumen adalah mengedepankan kualitas, harga bersaing, dan menjamin produk yang dihasilkan dengan layanan after sales.</p>
                    
                </div>
            </div>
            <div class="col-md-6 order-1 order-md-2">
                <div class="about-image space__bottom__lm--30">
                    <img width="671" height="408" src="assets/img/about/visimisi1.webp" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
    <!--====================  End of about area  ====================-->
    <!--====================  feature area ====================-->
    <div class="feature-area space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 order-2 order-lg-1">
                    <!-- feature content wrapper -->
                    <div class="feature-content-wrapper space__bottom--m35">
                        <div class="single-feature space__bottom--35">
                            <div class="single-feature__icon">
                                <img width="53" height="53" src="assets/img/icons/visiicon.webp" class="img-fluid" alt="">
                            </div>
                            <div class="single-feature__content">
                                <h4 class="single-feature__title">Visi</h4>
                                <p class="single-feature__text">Menjadi perusahaan yang unggul dalam bidang manufaktur permesinan yang dapat memenuhi kebutuhan customer</p>
                            </div>
                        </div>
                        <div class="single-feature space__bottom--35">
                            <div class="single-feature__icon">
                                <img width="53" height="53" src="assets/img/icons/misiicon.webp" class="img-fluid" alt="">
                            </div>
                            <div class="single-feature__content">
                                <h4 class="single-feature__title">Misi</h4>
                                <p class="single-feature__text">1. Mengutamakan kualitas dan kepuasan customer, serta menjamin produk dengan layanan after sales<br>
                                <br>2. Menjadi perusahaan yang dapat memenuhi standar kebutuhan manufaktur<br>
                                <br>3. Meningkatkan target pasar perusahaan baik secara nasional dan juga internasional
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 space__bottom__md--40 space__bottom__lm--40 order-1 order-lg-2">
                    <!-- feature content image -->
                    <div class="feature-content-image">
                        <img width="338" height="367" src="assets/img/feature/misi02.webp" class="img-fluid" alt="">
                        <img width="301" height="372" src="assets/img/feature/visi01.webp" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of feature area  ====================-->
@endsection
