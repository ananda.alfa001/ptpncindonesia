@extends('component.main')
@section('content')
    <!--box atas-->
    <div class="page-breadcrumb bg-img space__bottom--r120" data-bg="assets/img/backgrounds/bc-bg.webp">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="page-breadcrumb-content text-center">
                        <h1>STRUKTUR ORGANISASI</h1>
                        <ul class="page-breadcrumb-links">
                            <li><a href="#">PT PIONEER CNC INDONESIA</a></li>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end box atas-->
    <!--====================  Struktur details area ====================-->
    <div class="project-section space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-12 space__bottom--40">
                    <div class="project-image"><img width="1170" height="2273" src="assets/img/team/struktur.webp" class="img-fluid" alt=""></div>
                </div>
                
    <!--====================  End of project details area  ====================-->
@endsection
