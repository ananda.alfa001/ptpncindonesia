@extends('component.main')
@section('content')
    <!--====================  hero slider area ====================-->
    <div class="hero-area space__bottom--r120">
        <div class="hero-area-wrapper">
            <div class="single-hero-slider single-hero-slider--background position-relative bg-img"
                data-bg="assets/img/hero-slider/home2-slider1-image2.webp">
                <!-- hero slider content -->
                <div class="single-hero-slider__abs-img">
                    <img width="977" height="839" src="assets/img/hero-slider/home2-slider1.webp" class="img-fluid"
                        alt="">
                    <img width="978" height="849" src="assets/img/hero-slider/home2-slider2.webp" class="img-fluid"
                        alt="">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="hero-slider-content hero-slider-content--extra-space">
                                <h3
                                    class="hero-slider-content__subtitle hero-slider-content__subtitle hero-slider-content__subtitle--dark">
                                    PT Pioneer CNC Indonesia</h3>
                                <h4 class="hero-slider-content__title hero-slider-content__title--dark space__bottom--50">
                                    best quality
                                    for <br>the excellents
                                </h4>
                                <a href="{{ route('home.contact') }}" class="default-btn default-btn--hero-slider">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of hero slider area  ====================-->
    <!--====================  feature area ====================-->
    <div class="feature-area space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-xl-6 space__bottom__md--40 space__bottom__lm--40">
                    <!-- feature content image -->
                    <div class="feature-content-image">
                        <img width="338" height="367" src="assets/img/feature/feature-banner-1.webp" class="img-fluid"
                            alt="">
                        <img width="301" height="372" src="assets/img/feature/feature-banner-2.webp" class="img-fluid"
                            alt="">
                    </div>
                </div>
                <div class="col-lg-6 col-xl-5 offset-xl-1">
                    <!-- feature content wrapper -->
                    <div class="feature-content-wrapper space__bottom--m40">
                        <div class="single-feature space__bottom--40">
                            <div class="single-feature__icon">
                                <img width="53" height="53" src="assets/img/icons/feature-1.webp" class="img-fluid"
                                    alt="">
                            </div>
                            <div class="single-feature__content">
                                <h4 class="single-feature__title">TKDN</h4>
                                <p class="single-feature__text">TKDN (Tingkat Komponen Dalam Negeri) merupakan besarnya
                                    komponen dalam negeri pada barang, jasa dan gabungan barang dan jasa.</p>
                            </div>
                        </div>
                        <div class="single-feature space__bottom--40">
                            <div class="single-feature__icon">
                                <img width="53" height="53" src="assets/img/icons/feature-2.webp" class="img-fluid"
                                    alt="">
                            </div>
                            <div class="single-feature__content">
                                <h4 class="single-feature__title">Kualitas Terbaik</h4>
                                <p class="single-feature__text">Kami selalu menyediakan produk yang berkualitas dan
                                    menggunakan bahan bermutu tinggi untuk para konsumen</p>
                            </div>
                        </div>
                        <div class="single-feature space__bottom--40">
                            <div class="single-feature__icon">
                                <img width="53" height="53" src="assets/img/icons/feature-3.webp" class="img-fluid"
                                    alt="">
                            </div>
                            <div class="single-feature__content">
                                <h4 class="single-feature__title">Mesin CNC Fiber Laser</h4>
                                <p class="single-feature__text">Tim kami telah melakukan riset dengan cermat untuk
                                    menghadirkan mesin cnc fiber laser sebagai produk unggulan dengan desain yang lebih
                                    futuristik dan tentunya sangat mampu digunakan untuk high speed cutting.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of feature area  ====================-->
    <!--====================  about area ====================-->
    <div class="about-area space__bottom--r120 ">
        <div class="container">
            <div class="row align-items-center row-25">
                <div class="col-md-6 order-2 order-md-1">
                    <div class="about-content">
                        <!-- section title -->
                        <div class="section-title space__bottom--25">
                            <h3 class="section-title__sub">Produk Unggulan</h3>
                            <h2 class="section-title__title">Mesin CNC Fiber Laser Cutting</h2>
                        </div>
                        <p class="about-content__text space__bottom--40">Tim kami telah melakukan riset dengan cermat untuk
                            menghadirkan mesin cnc fiber laser generasi ke-2 dengan harga terjangkau untuk umkm.
                            Customer pun bisa mendapatkan mesin cnc fiber laser kami dengan desain yang lebih futuristik
                            dan tentunya sangat mampu digunakan untuk high speed cutting.</p>
                        <a href="#" class="default-btn">Jelajahi Produk Kami</a>
                    </div>
                </div>
                <div class="col-md-6 order-1 order-md-2">
                    <div class="about-image space__bottom__lm--30">
                        <img width="671" height="408" src="assets/img/about/about-section-2.webp" class="img-fluid"
                            alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of about area  ====================-->
    <!--====================  service area ====================-->

    <!--====================  End of service area  ====================-->
    <!--====================  cta area ====================-->
    <div class="cta-area cta-area-bg bg-img" data-bg="assets/img/backgrounds/cta-bg2.webp">
        <div class="cta-wrapper background-color--dark-overlay space__inner__top--50 space__inner__bottom--150">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 mx-auto">
                        <div class="cta-block cta-block--default-color">
                            <p class="cta-block__light-text text-center">PT Pioneer CNC Indonesia adalah perusahaan bidang manufaktur Mesin CNC sejak tahun<span> 2015</span> </p>
                            <p class="cta-block__semi-bold-text cta-block__semi-bold-text--medium text-center">Aktif melayani banyak customer dari berbagai wilayah di Indonesia dari Sabang sampai Merauke.
                                </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of cta area  ====================-->
    <!-- funfact include -->
    <div class="funfact-wrapper space__top--m100">
        <!--====================  fun fact area ====================-->
        <div class="fun-fact-area space__bottom--r120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- fun fact wrapper -->
                        <div class="fun-fact-wrapper fun-fact-wrapper-bg bg-img"
                            data-bg="assets/img/backgrounds/funfact-bg.webp">
                            <div class="fun-fact-inner background-color--default-overlay background-repeat--x-bottom space__inner--y30 bg-img"
                                data-bg="assets/img/icons/ruler-black.webp">
                                <div class="fun-fact-content-wrapper">
                                    <div class="single-fun-fact">
                                        <h3 class="single-fun-fact__number counter">550</h3>
                                        <h4 class="single-fun-fact__text">Produk Terjual</h4>
                                    </div>
                                    <div class="single-fun-fact">
                                        <h3 class="single-fun-fact__number counter">350</h3>
                                        <h4 class="single-fun-fact__text">Clients</h4>
                                    </div>
                                    <div class="single-fun-fact">
                                        <h3 class="single-fun-fact__number counter">550</h3>
                                        <h4 class="single-fun-fact__text">Sukses</h4>
                                    </div>
                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  End of fun fact area  ====================-->
    </div>
    <!--====================  project area ====================-->
    <div class="project-area space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- section title -->
                    <div class="section-title text-center  space__bottom--40 mx-auto">
                        <h3 class="section-title__sub">Katalog Produk Kami</h3>
                        <h2 class="section-title__title">Berbagai produk hasil inovasi kami untuk anda</h2>
                    </div>
                </div>
            </div>
            <div class="project-wrapper project-wrapper--masonry row space__bottom--m30">
                <div class="col-1 gutter"></div>
                <div class="col-lg-4 col-md-6 grid-item space__bottom--30">
                    <div class="single-project-wrapper single-project-wrapper--reduced-abs">
                        <a class="single-project-item p-0" href="#">
                            <img width="370" height="600" src="assets/img/projects/project9.webp" class="img-fluid"
                                alt="">
                            <span class="single-project-title">Mesin CNC Fiber Laser Cutting</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 grid-item space__bottom--30">
                    <div class="single-project-wrapper single-project-wrapper--reduced-abs">
                        <a class="single-project-item p-0" href="#">
                            <img width="470" height="285" src="assets/img/projects/project10.webp"
                                class="img-fluid" alt="">
                            <span class="single-project-title">Mesin CNC Plasma Cutting</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 grid-item space__bottom--30">
                    <div class="single-project-wrapper single-project-wrapper--reduced-abs">
                        <a class="single-project-item p-0" href="#">
                            <img width="270" height="285" src="assets/img/projects/project11.webp"
                                class="img-fluid" alt="">
                            <span class="single-project-title">Mesin CNC Router</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-5 grid-item space__bottom--30">
                    <div class="single-project-wrapper single-project-wrapper--reduced-abs">
                        <a class="single-project-item p-0" href="#">
                            <img width="270" height="285" src="assets/img/projects/project12.webp"
                                class="img-fluid" alt="">
                            <span class="single-project-title">Mesin CNC Rotary Cutting</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-7 grid-item space__bottom--30">
                    <div class="single-project-wrapper single-project-wrapper--reduced-abs">
                        <a class="single-project-item p-0" href="#">
                            <img width="470" height="285" src="assets/img/projects/project13.webp"
                                class="img-fluid" alt="">
                            <span class="single-project-title">Mesin CNC Plano Milling</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of project area  ====================-->
    <!--====================  team area ====================-->
    <div class="team-area space__bottom--r120 position-relative">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 space__bottom__md--40 space__bottom__lm--40">
                    <div class="team-member-title-wrapper">
                        <!-- section title -->
                        <div class="section-title space__bottom--30 space__bottom__md--30 space__bottom__lm--20">
                            <h3 class="section-title__sub">Tim Kami</h3>
                            <h2 class="section-title__title">Solid & Terbaik</h2>
                        </div>
                        <p class="team-text space__bottom--40 space__bottom__md--30 space__bottom__lm--20">
                            Kami pun terus berbenah dengan sistem kerja yang kini lebih kondusif dan efisien demi
                            terwujudnya produk mesin terbaik bagi para customer yang luar biasa.
                        </p>
                        <a href="#" class="default-btn">View more</a>
                    </div>
                </div>
                <div class="col-lg-8 team-slider-column-wrapper">
                    <!-- team member slider -->
                    <div class="team-slider-wrapper slick-slider-x-gap-30">
                        <div class="single-team-member text-center">
                            <div class="single-team-member__image space__bottom--10">
                                <img width="510" height="776" src="assets/img/team/team-member1.webp" alt="">
                            </div>
                            <h5 class="single-team-member__name">Syaiful Adhim</h5>
                            <p class="single-team-member__des">Direktur</p>
                        </div>
                        <div class="single-team-member text-center">
                            <div class="single-team-member__image space__bottom--10">
                                <img width="510" height="775" src="assets/img/team/team-member2.webp" alt="">
                            </div>
                            <h5 class="single-team-member__name"></h5>
                            <p class="single-team-member__des"></p>
                        </div>
                        <div class="single-team-member text-center">
                            <div class="single-team-member__image space__bottom--10">
                                <img width="510" height="775" src="assets/img/team/team-member3.webp" alt="">
                            </div>
                            <h5 class="single-team-member__name"></h5>
                            <p class="single-team-member__des"></p>
                        </div>
                        <div class="single-team-member text-center">
                            <div class="single-team-member__image space__bottom--10">
                                <img width="510" height="775" src="assets/img/team/team-member4.webp" alt="">
                            </div>
                            <h5 class="single-team-member__name"></h5>
                            <p class="single-team-member__des"></p>
                        </div>
                        <div class="single-team-member text-center">
                            <div class="single-team-member__image space__bottom--10">
                                <img width="510" height="776" src="assets/img/team/team-member5.webp" alt="">
                            </div>
                            <h5 class="single-team-member__name"></h5>
                            <p class="single-team-member__des"></p>
                        </div>
                        <div class="single-team-member text-center">
                            <div class="single-team-member__image space__bottom--10">
                                <img width="510" height="775" src="assets/img/team/team-member6.webp" alt="">
                            </div>
                            <h5 class="single-team-member__name"></h5>
                            <p class="single-team-member__des"></p>
                        </div>
                        <div class="single-team-member text-center">
                            <div class="single-team-member__image space__bottom--10">
                                <img width="510" height="775" src="assets/img/team/team-member7.webp" alt="">
                            </div>
                            <h5 class="single-team-member__name"></h5>
                            <p class="single-team-member__des"></p>
                        </div>
                        <div class="single-team-member text-center">
                            <div class="single-team-member__image space__bottom--10">
                                <img width="510" height="775" src="assets/img/team/team-member8.webp" alt="">
                            </div>
                            <h5 class="single-team-member__name"></h5>
                            <p class="single-team-member__des"></p>
                        </div>
                        <div class="single-team-member text-center">
                            <div class="single-team-member__image space__bottom--10">
                                <img width="510" height="775" src="assets/img/team/team-member10.webp" alt="">
                            </div>
                            <h5 class="single-team-member__name"></h5>
                            <p class="single-team-member__des"></p>
                        </div>
                        <div class="single-team-member text-center">
                            <div class="single-team-member__image space__bottom--10">
                                <img width="510" height="775" src="assets/img/team/team-member11.webp" alt="">
                            </div>
                            <h5 class="single-team-member__name"></h5>
                            <p class="single-team-member__des"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--====================  End of team area  ====================-->
    <!--====================  testimonial area ====================-->
    <div class="testimonial-area space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- section title -->
                    <div class="section-title text-center  space__bottom--40 mx-auto">
                        <h3 class="section-title__sub">Testimoni</h3>
                        <h2 class="section-title__title">Apa pendapat konsumen tentang kami?</h2>
                    </div>
                    <!-- testimonial slider -->
                    <!--<div
                        class="testimonial-multi-slider-wrapper space__inner__bottom--50 space__inner__bottom__md--50 space__inner__bottom__lm--50">
                        <div class="single-testimonial single-testimonial--style2">
                            <p class="single-testimonial__text space__bottom--20"> <span class="quote-left">"</span>
                                # <span
                                    class="quote-right">"</span></p>
                            <h5 class="single-testimonial__author">#</h5>
                            <p class="single-testimonial__author-des">#</p>
                        </div>
                        <div class="single-testimonial single-testimonial--style2">
                            <p class="single-testimonial__text space__bottom--20"> <span class="quote-left">"</span>
                                # <span class="quote-right">"</span></p>
                            <h5 class="single-testimonial__author">#</h5>
                            <p class="single-testimonial__author-des">#</p>
                        </div>
                        <div class="single-testimonial single-testimonial--style2">
                            <p class="single-testimonial__text space__bottom--20"> <span class="quote-left">"</span>
                                # <span class="quote-right">"</span>
                            </p>
                            <h5 class="single-testimonial__author">#</h5>
                            <p class="single-testimonial__author-des">#</p>
                        </div>
                        <div class="single-testimonial single-testimonial--style2">
                            <p class="single-testimonial__text space__bottom--20"> <span class="quote-left">"</span>
                                # <span class="quote-right">"</span></p>
                            <h5 class="single-testimonial__author">#</h5>
                            <p class="single-testimonial__author-des">#</p>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of testimonial area  ====================-->
    <!--====================  blog grid slider area ====================-->

    <!--====================  End of blog grid slider area  ====================-->
    <!--====================  brand logo area ====================-->
    <div class="brand-logo-area space__bottom--r120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- brand logo slider -->
                    <div class="brand-logo-wrapper">
                        <div class="single-brand-logo">
                            <a href="#"><img width="126" height="50" src="assets/img/brand-logo/logo1.webp"
                                    class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img width="129" height="49" src="assets/img/brand-logo/logo2.webp"
                                    class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img width="113" height="49" src="assets/img/brand-logo/logo3.webp"
                                    class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img width="339" height="100" src="assets/img/brand-logo/logo4.webp"
                                    class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img width="125" height="100" src="assets/img/brand-logo/logo5.webp"
                                    class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img width="270" height="100" src="assets/img/brand-logo/logo6.webp"
                                    class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img width="225" height="100" src="assets/img/brand-logo/logo7.webp"
                                    class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img width="94" height="100" src="assets/img/brand-logo/logo8.webp"
                                    class="img-fluid" alt=""></a>
                        </div>
                        <div class="single-brand-logo">
                            <a href="#"><img width="101" height="100" src="assets/img/brand-logo/logo9.webp"
                                    class="img-fluid" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of brand logo area  ====================-->
    <!--====================  newsletter area ====================-->
    <div class="newsletter-area newsletter-area-bg bg-img" data-bg="assets/img/backgrounds/newsletter-bg.webp">
        <div class="newsletter-wrapper background-color--default-overlay space__inner--y60">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-xl-10 mx-auto">
                        <div class="row align-items-center">
                            <div class="col-lg-4 mb-3 mb-lg-0">
                                <!-- newsletter title -->
                                <h3 class="newsletter-title"><span>Best quality</span> for the excellents</h3>
                            </div>
                            <div class="col-lg-8">

                                <!-- mailchimp-alerts Start -->

                                <!-- mailchimp-alerts end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--====================  End of newsletter area  ====================-->
@endsection
